import os
from numpy import *
import re
import math
import matplotlib
matplotlib.use('Agg')

# this line has to be after previous line because we don't have $DISPLAY environment
import matplotlib.pyplot as plt
from pylab import *

class MultipleRunLogs:
    def __init__(self):
        self.run_logs = []

    def add_log(self, run_log):
        self.run_logs.append(run_log)

    def final_score(self, array):
        value = 0.0
        for val in array:
            value += val
        return value / len(array)

    def time_to_epoch(self, epoch_number):
        times = []
        for i in range(len(self.run_logs)):
            times.append( self.run_logs[i].time_to_epoch(epoch_number) )

        return self.final_score(times)

    def agg_time_to_epoch(self, epoch_number):
        times = []
        for i in range(len(self.run_logs)):
            times.append( self.run_logs[i].agg_time_to_epoch(epoch_number) )

        return self.final_score(times)

    def training_error_for_epoch_with_min_and_max(self, epoch_number):
        errors = []
        for i in range(len(self.run_logs)):
            errors.append( self.run_logs[i].training_error_for_epoch(epoch_number) )

        errors.sort()
        return (self.final_score(errors), errors[0], errors[ len(errors) - 1 ])

    def test_error_for_epoch_with_min_and_max(self, epoch_number):
        errors = []
        for i in range(len(self.run_logs)):
            errors.append( self.run_logs[i].test_error_for_epoch(epoch_number) )

        errors.sort()
        return (self.final_score(errors), errors[0], errors[ len(errors) - 1 ])

    def test_top5_for_epoch_with_min_and_max(self, epoch_number):
        errors = []
        for i in range(len(self.run_logs)):
            errors.append( self.run_logs[i].test_top5_for_epoch(epoch_number) )

        errors.sort()
        return (self.final_score(errors), errors[0], errors[ len(errors) - 1 ])

    def minimum_test_accuracy(self, epoch_number):
        errors = []
        for i in range(len(self.run_logs)):
            errors.append( self.run_logs[i].min_with_id(epoch_number) )

        errors.sort()
        # return mid element
        return errors[ len(errors) // 2 ]

    def minimum_top5_accuracy(self, epoch_number):
        errors = []
        for i in range(len(self.run_logs)):
            errors.append( self.run_logs[i].min_top5_with_id(epoch_number) )

        errors.sort()
        # return mid element
        return errors[ len(errors) // 2 ]

class RunLog:
    def __init__(self):
        self.number_of_epochs = 0
        self.training_error_per_epoch = []
        self.test_error_per_epoch = []
        self.test_top5_per_epoch = []
        self.training_time_per_epoch = []
        self.aggregation_time_per_epoch = []

    def add_to_list(self, list, value, position, add = False):
        while (len(list) < position):
            list.append(value)

        if (add):
            list[position - 1] += value

    def time_to_epoch(self, epoch_number):
        time = 0.0
        for i in range(epoch_number):
            time += self.training_time_per_epoch[i]
        return time

    def agg_time_to_epoch(self, epoch_number):
        time = 0.0
        for i in range(epoch_number):
            time += self.aggregation_time_per_epoch[i]
        return time

    def training_error_for_epoch(self, epoch_number):
        return self.training_error_per_epoch[epoch_number]

    def test_error_for_epoch(self, epoch_number):
        return self.test_error_per_epoch[epoch_number]

    def test_top5_for_epoch(self, epoch_number):
        return self.test_top5_per_epoch[epoch_number]

    def min_with_id(self, epoch_number):
        min_value = self.test_error_per_epoch[0]
        min_id = 0

        for i in range(1, epoch_number):
            if (min_value > self.test_error_per_epoch[i]):
                min_value = self.test_error_per_epoch[i]
                min_id = i

        return (min_value, min_id)

    def min_top5_with_id(self, epoch_number):
        min_value = self.test_top5_per_epoch[0]
        min_id = 0

        for i in range(1, epoch_number):
            if (min_value > self.test_top5_per_epoch[i]):
                min_value = self.test_top5_per_epoch[i]
                min_id = i

        return (min_value, min_id)

##################################
# const for logs
##################################
log_path = "./data/"
output_dir = "./plots/"
training_error_name = "ce = "
test_error_name = "errs = "
test_top5_name = "top5Errs = "

logs = dict()

def DrawFigure(time_graph, ylabel, xlabel, output_directory, image_name, logs, is_training_graph):
#for figure_type in ['with_variance', 'with_marker', 'without_marker']:
    fig = plt.figure()
    # ax = fig.add_subplot(111)
    ax = fig.add_axes((.1,.4,.8,.5))

    aggregation_text = ""
    for dict_key in sorted(logs.keys()):
        x = []
        y = []
        lower_y = []
        upper_y = []

        x2 = []
        y2 = []
        lower_y2 = []
        upper_y2 = []

        num_of_epochs = len(logs[dict_key].run_logs[0].training_error_per_epoch)
        print(dict_key, num_of_epochs)
        for i in range(num_of_epochs):
            if (is_training_graph):
                (value, minimum, maximum) = logs[dict_key].training_error_for_epoch_with_min_and_max(i)
            else:
                (value, minimum, maximum) = logs[dict_key].test_error_for_epoch_with_min_and_max(i)
                (value2, minimum2, maximum2) = logs[dict_key].test_top5_for_epoch_with_min_and_max(i)

            if (time_graph):
                time = logs[dict_key].time_to_epoch(i)
            else:
                time = i + 1

            y.append(value)
            if (not is_training_graph):
                y[-1] = 100 - y[-1]
            lower_y.append(value - minimum)
            upper_y.append(maximum - value)
            x.append(time)

            if (not is_training_graph):
                y2.append(value2)
                if (not is_training_graph):
                    y2[-1] = 100 - y2[-1]
                lower_y2.append(value2 - minimum2)
                upper_y2.append(maximum2 - value2)
                x2.append(time)

            if (lower_y[-1] > 0.01) or (upper_y[-1] > 0.01):
                print()
                print(dict_key)
                print("Epoch ", i)
                print(minimum)
                print(value)
                print(maximum)

        total_time = logs[dict_key].time_to_epoch(num_of_epochs - 1)
        agg_time = 0 # logs[dict_key].agg_time_to_epoch(num_of_epochs - 1)
        min_error = logs[dict_key].minimum_test_accuracy(num_of_epochs - 1)
        min_top5_error = logs[dict_key].minimum_top5_accuracy(num_of_epochs - 1)
        error = logs[dict_key].training_error_for_epoch_with_min_and_max(num_of_epochs - 1)[0]

        aggregation_text += dict_key

        if (is_training_graph):
            aggregation_text += " - accuracy = {0:.2f}".format(error)
            aggregation_text += " | time = {0:d}h {1:d}m {2:d}s\n".format(int(total_time)//3600, int(total_time) % 3600 // 60, int(total_time) % 60)
            # aggregation_text += " | agg_time = {0:.2f}s ({1:0.2f}%)\n".format(agg_time,agg_time / total_time * 100)
        else:
            aggregation_text += " - max_accuracy = {0:.2f}% (in epoch {1})".format(100 - min_error[0],min_error[1] + 1)
            aggregation_text += " | max_top5_accuracy = {0:.2f}% (in epoch {1})\n".format(100 - min_top5_error[0],min_top5_error[1] + 1)

        # ax.plot(x, y)

        #if (figure_type == "with_variance"):
        ax.plot(x, y, label=dict_key)
        if (not is_training_graph):
            eb=ax.plot(x2, y2, linestyle='--', label=dict_key + "_top5")
        #if (figure_type == "with_marker"):
        # ax.plot(x, y, linestyle='solid', linewidth=1.2, marker=".", markersize=3)
        #if (figure_type == "without_marker"):
        # ax.plot(x, y, linestyle='solid', linewidth=1.2)

    print(aggregation_text)

    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)

    # Put a legend to the right of the current axis
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    # ax.legend(sorted(logs.keys()))

    tmp = fig.text(.1,0, aggregation_text)

    plt.savefig(output_directory + "/" + image_name, bbox_extra_artists=(lgd,tmp), bbox_inches='tight')
    #if not os.path.exists(output_directory + "/" + figure_type + "/"):
    #    os.makedirs(output_directory + "/" + figure_type + "/")
    #plt.savefig(output_directory + "/" + figure_type + "/" + image_name)
    # plt.savefig(output_directory + "/" + image_name)



## int main()

# read all files in directory logs
for filename in sorted(os.listdir(log_path)):
    print('Working on file ', filename)

    # example of log file 2bit_1.log, 2bit_2.log, ...
    # dict_key = filename[:filename.rfind("_")]
    dict_key = filename

    curr_epoch = 0
    run_log = RunLog()
    
    with open(log_path + filename) as f:

        for line_in_file in f:
            line_stripped = line_in_file.strip()

            # find current epoch
            for start_idx in [m.start() for m in re.finditer('Finished Epoch', line_stripped)]:
                line = line_stripped[ start_idx : ]
                try:
                    curr_epoch = (int)(line[ line.find("[") + 1 : line.find("of") - 1 ].strip())
                except:
                    pass

            # find training error
            if (line_stripped.find("[Training]") != -1):
                for start_idx in [m.start() for m in re.finditer(training_error_name, line_stripped)]:
                    line = line_stripped[ start_idx : ]
                    try:
                        error = (float)(line[ line.find(training_error_name) + len(training_error_name) : line.find("*", line.find(training_error_name)) - 1 ])
                        run_log.add_to_list(run_log.training_error_per_epoch, error, curr_epoch)
                    except:
                        print(log_path + filename)
                        print(curr_epoch)
                        pass

            # find epoch time
            for start_idx in [m.start() for m in re.finditer('epochTime', line_stripped)]:
                line = line_stripped[ start_idx : ]
                try:
                    epoch_time = (float)(line[ line.find("epochTime=") + 10 : line.find("s", line.find("epochTime=") - 1) ])
                    run_log.add_to_list(run_log.training_time_per_epoch, epoch_time, curr_epoch)
                except:
                    pass

            # find test error
            if (line_stripped.find('[Validate]') != -1):
                for start_idx in [m.start() for m in re.finditer(test_error_name, line_stripped)]:
                    line = line_stripped[ start_idx : ]
                    try:
                        if (line.find('%') == -1):
                            error = (float)(line[ line.find(test_error_name) + len(test_error_name) : line.find("*", line.find(test_error_name)) - 1 ])
                        else:
                            error = (float)(line[ line.find(test_error_name) + len(test_error_name) : line.find("%", line.find(test_error_name)) - 1 ])
                        run_log.add_to_list(run_log.test_error_per_epoch, error, curr_epoch)
                    except:
                        print(log_path + filename)
                        print(curr_epoch)
                        pass

                for start_idx in [m.start() for m in re.finditer(test_top5_name, line_stripped)]:
                    line = line_stripped[ start_idx : ]
                    try:
                        if (line.find('%') == -1):
                            error = (float)(line[ line.find(test_top5_name) + len(test_top5_name) : line.find("*", line.find(test_top5_name)) - 1 ])
                        else:
                            error = (float)(line[ line.find(test_top5_name) + len(test_top5_name) : line.find("%", line.find(test_top5_name)) - 1 ])
                        run_log.add_to_list(run_log.test_top5_per_epoch, error, curr_epoch)
                    except:
                        print(log_path + filename)
                        print(curr_epoch)
                        pass

            # find aggregation time
            for start_idx in [m.start() for m in re.finditer('Actual gradient aggregation time: ', line_stripped)]:
                line = line_stripped[ start_idx : ]
                try:
                    time = (float)(line[ line.find('Actual gradient aggregation time: ') + len('Actual gradient aggregation time: ') :])
                    run_log.add_to_list(run_log.aggregation_time_per_epoch, 10 * time / 2, curr_epoch + 1, True)
                except:
                    pass
        

        if (dict_key not in logs.keys()):
            logs[dict_key] = MultipleRunLogs()

        logs[dict_key].add_log(run_log)

DrawFigure(True, "Training error accuracy", "Time in seconds", output_dir, "train_per_time.png", logs, True)
DrawFigure(False, "Training error accuracy", "Epoch number", output_dir, "train_per_epoch.png", logs, True)
DrawFigure(True, "Test accuracy", "Time in seconds", output_dir, "test_per_time.png", logs, False)
DrawFigure(False, "Test accuracy", "Epoch number", output_dir, "test_per_epoch.png", logs, False)


# with open(output_dir + 'numbers_all.txt', 'w') as f:
#     for dict_key in sorted(logs.keys()):
#         f.write(dict_key + "\n")

#         output_str = ""
#         num_of_epochs = len(logs[dict_key].run_logs[0].training_error_per_epoch)
#         for i in range(num_of_epochs):
#             output_str = str(i+1)
#             output_str += " "

#             for j in range(len(logs[dict_key].run_logs)):
#                 time = logs[dict_key].run_logs[j].time_to_epoch(i)
#                 train = logs[dict_key].run_logs[j].training_error_for_epoch(i)
#                 test = logs[dict_key].run_logs[j].test_error_for_epoch(i)

#                 output_str += str(time) + " " + str(train) + " " + str(test) + " "

#             f.write(output_str + "\n")

# with open(output_dir + 'matrix_test_maximum.txt', 'w') as f:
    
#     output_str = ""
#     for dict_key in sorted(logs.keys()):
#         output_str += dict_key + " "
#     f.write(output_str + "\n")

#     for i in range(20):
#         output_str = ""
#         for dict_key in sorted(logs.keys()):
#             (value, minimum, maximum) = logs[dict_key].test_error_for_epoch_with_min_and_max(i)
#             output_str += str(maximum) + " "

#         f.write(output_str + "\n")
